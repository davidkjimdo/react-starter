/*eslint-disable */
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Layout from '../Layout/index';
import MainWrapper from './MainWrapper';

import LogIn from '../LogIn/index';
import ExamplePageOne from '../Example/index';
import ExamplePageTwo from '../ExampleTwo/index';
import config from '../../config'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
  localStorage.getItem(config.TOKEN_LOCAL_STORAGE) != null ?
      <Component {...props} />
    :
      <Redirect to={{
        pathname: '/',
        state: { from: props.location }
        }}
      />
  )} />
);

const Pages = () => (
  <Switch>
    <PrivateRoute path="/home"      component={ExamplePageOne} />
    <PrivateRoute path="/pages/one" component={ExamplePageOne} />
    <PrivateRoute path="/pages/two" component={ExamplePageTwo} />
  </Switch>
);

const wrappedRoutes = () => (
  <div>
    <Layout />
    <div className="container__wrap">
      <Route path="/pages" component={Pages} />
    </div>
  </div>
);

const Router = () => (
  <MainWrapper>
    <main>
      <Switch>
        <Route exact path="/" component={LogIn} />
        <Route exact path="/log_in" component={LogIn} />
        // <Route path="/" component={wrappedRoutes} />
      </Switch>
    </main>
  </MainWrapper>
);

export default Router;
