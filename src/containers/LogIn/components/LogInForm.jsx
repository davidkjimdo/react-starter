/*eslint-disable */

import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import EyeIcon from 'mdi-react/EyeIcon';
import KeyVariantIcon from 'mdi-react/KeyVariantIcon';
import AccountOutlineIcon from 'mdi-react/AccountOutlineIcon';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Redirect } from 'react-router';
import config from '../../../config'
import Notifications, {notify} from 'react-notify-toast';

class LogInForm extends Component {

  constructor() {
    super();
    this.state = {
      showPassword: false,
      token: false,
      password: '',
      username: ''
    };
  }

  componentDidMount() {
    if (localStorage.getItem(config.TOKEN_LOCAL_STORAGE)) {
      this.setState({
        token: localStorage.getItem(config.TOKEN_LOCAL_STORAGE),
      });
    }
  }

  showPassword = (e) => {
    e.preventDefault();
    this.setState(prevState => ({ showPassword: !prevState.showPassword }));
  };

componentDidUpdate() {
  if (this.state.token) {
    return <Redirect to='/pages/one'/>;
  }
}

  login() {
    const app = this;

    axios.post(config.API_URL + 'login', {
      email: app.state.username,
      password: app.state.password
    }).then(function (response) {
        if (response.data.hasOwnProperty('token')) {
         const token = response.data.token
         localStorage.setItem(config.TOKEN_LOCAL_STORAGE, token);
         app.setState({
            token: token
         })
        }
     })
     .catch(error => {
       notify.show(error.response.data.message, 'error')
      })
  }

  render() {
    const { showPassword, token } = this.state;

    if (token) {
      return <Redirect to='/home'/>;
    }

    return (
      <form className="form">
        <div className="form__form-group">
          <span className="form__form-group-label">Username</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <AccountOutlineIcon />
            </div>
            <Field
              name="name"
              component="input"
              type="text"
              placeholder="Name"
              value={this.state.username}
              onChange={ (e) => this.setState({username: e.target.value}) }
            />
          </div>
        </div>
        <div className="form__form-group">
          <span className="form__form-group-label">Mot de passe {this.state.password} </span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <KeyVariantIcon />
            </div>
            <Field
              name="password"
              component="input"
              type={showPassword ? 'text' : 'password'}
              placeholder="Password"
              value={this.state.password}
              onChange={ (e) => this.setState({password: e.target.value}) }
            />
            <button
              className={`form__form-group-button${showPassword ? ' active' : ''}`}
              onClick={e => this.showPassword(e)}
              type="submit"
            ><EyeIcon />
            </button>
          </div>
        </div>
        <button
          className="btn btn-primary account__btn account__btn--small"
          onClick={e => this.login()}
          type="button"
        >Se connecter
        </button>
      </form>
    );
  }
}

export default reduxForm({
  form: 'log_in_form',
})(LogInForm);
